type RangeNum = number & { __inclusive?: boolean; __exclusive?: boolean };

export const inclusive = (num: number) =>
  Object.assign(num, { __inclusive: true });
export const incl = inclusive;
const isInclusive = (num: number | RangeNum) =>
  (num as RangeNum).__inclusive ?? false;

export const exclusive = (num: number) =>
  Object.assign(num, { __exclusive: true });
export const excl = exclusive;
const isExclusive = (num: number | RangeNum) =>
  (num as RangeNum).__exclusive ?? false;

export const range = Object.assign(
  (start: number | RangeNum, end: number | RangeNum, step?: number) => ({
    [Symbol.iterator]: function* () {
      const expectedSign = Math.sign(end - start);
      if (typeof step === "undefined") {
        step = expectedSign;
      }
      if (Math.sign(step) !== expectedSign) {
        throw new Error("Step must be in the same direction as start and end");
      }
      const isStartExclusive = isExclusive(start);
      const isEndInclusive = isInclusive(end);
      if (isStartExclusive) {
        start += step;
      }

      // convert to numbers (in case they are RangeNums):
      start = +start;
      end = +end;

      let i = start;
      for (; expectedSign >= 0 ? i < end : i > end; i += step) {
        yield i as number;
      }

      if (isEndInclusive && i === end) {
        yield end as number;
      }
    },
  }),
  {
    incl,
    inclusive,
    excl,
    exclusive,
  },
);
