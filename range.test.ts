import { describe, it, expect } from "bun:test";
import { range } from "./range";

describe("range", () => {
  it("should return a range of numbers", () => {
    expect([...range(1, 10)]).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });
  it("should default to a step of 1", () => {
    expect([...range(1, 10)]).toEqual([...range(1, 10, 1)]);
  });
  it("should allow a step of 2", () => {
    expect([...range(1, 10, 2)]).toEqual([1, 3, 5, 7, 9]);
  });
  it("should allow a step of -1", () => {
    expect([...range(10, 1, -1)]).toEqual([10, 9, 8, 7, 6, 5, 4, 3, 2]);
  });
  it("should allow a step of -2", () => {
    expect([...range(10, 1, -2)]).toEqual([10, 8, 6, 4, 2]);
  });
  it("should allow an exclusive start", () => {
    expect([...range(range.excl(1), 10)]).toEqual([2, 3, 4, 5, 6, 7, 8, 9]);
  });
  it("should allow an inclusive end", () => {
    expect([...range(1, range.incl(10))]).toEqual([
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    ]);
  });
  it("should allow an exclusive start and inclusive end", () => {
    expect([...range(range.excl(1), range.incl(10))]).toEqual([
      2, 3, 4, 5, 6, 7, 8, 9, 10,
    ]);
  });
  it("should allow a negative step with an exclusive start and inclusive end", () => {
    expect([...range(range.excl(10), range.incl(0), -2)]).toEqual([
      8, 6, 4, 2, 0,
    ]);
  });
  it("should throw an error if the step is in the wrong direction", () => {
    expect(() => [...range(1, 10, -1)]).toThrow(
      "Step must be in the same direction as start and end",
    );
  });
  it("should be iterable in a for...of loop", () => {
    const nums: number[] = [];
    for (const num of range(1, 10)) {
      nums.push(num);
    }
    expect(nums).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9]);
  });
});
